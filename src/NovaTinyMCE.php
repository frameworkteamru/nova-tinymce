<?php

namespace Enmaboya\NovaTinyMCE;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class NovaTinyMCE extends Field
{
    public $language;
    public $component = 'Nova-TinyMCE';

        public function __construct(string $name, ?string $attribute = null, ?mixed $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->withMeta([
            'options' => [
                'path_absolute' => '/',
                'plugins' => [
                    'lists preview hr anchor pagebreak',
                    'wordcount fullscreen',
                    'contextmenu directionality',
                    'paste textcolor colorpicker textpattern'
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                'relative_urls' => false,
                'use_lfm' => false,
                'lfm_url' => 'laravel-filemanager'
            ]
        ]);
    }
    public function lang($lang){
        $this->language = $lang;
        return $this->withMeta([
            'lang' => $lang
        ]);
    }

    public function options(array $options)
    {
        $currentOptions = $this->meta['options'];
        
        return $this->withMeta([
            'options' => array_merge($currentOptions, $options)
        ]);
    }

    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        $model->setTranslation('translatable_value', $this->language, $request[$requestAttribute]);
        $model->save();
    }
}
